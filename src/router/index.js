import Vue from "vue";
import VueRouter from "vue-router";
import UserListings from "../views/UserListings.vue";
import UserDetails from "../views/UserDetails.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/users",
    name: "userListings",
    component: UserListings,
  },
  {
    path: "/users/:id",
    name: "userDetails",
    component: UserDetails,
    props: true
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
